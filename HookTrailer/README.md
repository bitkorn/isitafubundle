
# sendNotificationMessage
Damit der Hook getriggert wird, muss im Notification Center eine Benachrichtigung angelegt werden.

Dieser Benachrichtigung muss eine Nachricht angehangen werden.

Diese Nachricht benötigt ein Gateway. Ich habe ein Email Gateway verwendet.

Die Benachrichtigung habe ich in einem Formular im Formulargenerator ausgewählt, damit sie bei dem Formularversand getriggert wird.

Um die Benachrichtigung im Formular zur Auswahl zu haben, muss sie vom Typ Formularübertragung sein.

Also:

1. Benachrichtigungs Gateway anlegen
2. Benachrichtigung vom Typ Formularübertragung anlegen
3. Nachricht (mit Gateway) in der Benachrichtigung anlegen
4. Formular im Formulargenerator anlegen und darin die Benachrichtigung auswählen