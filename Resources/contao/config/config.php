<?php

$GLOBALS['TL_HOOKS']['replaceInsertTags'][] = array('Bitkorn\Isitafu\Resources\contao\AppCustomTags', 'myReplaceInsertTags');

/*
 * beides funktioniert:
 * 1. jeweils $GLOBALS['TL_CTE']
 * 2. mit array_insert()
 */
//$GLOBALS['TL_CTE']['bitkorn-isitafu']['dingdong'] = '\\Bitkorn\\Isitafu\\ContentElement\\Dingdong';
//$GLOBALS['TL_CTE']['bitkorn-isitafu']['onlyding'] = '\\Bitkorn\\Isitafu\\ContentElement\\OnlyDing';
array_insert($GLOBALS['TL_CTE'], 6,
        [
    'bitkorn-isitafu' => [
        'dingdong' => 'Bitkorn\\Isitafu\\ContentElement\\Dingdong',
        'onlyding' => 'Bitkorn\\Isitafu\\ContentElement\\OnlyDing'
    ]
]);
$GLOBALS['TL_HOOKS']['catalogManagerOverwriteQuery'][] = ['Bitkorn\Isitafu\HookTrailer\CatalogManagerOverwriteQueryProduct', 'execute'];
//$GLOBALS['TL_HOOKS']['generatePage'][] = ['Bitkorn\Isitafu\HookTrailer\CatalogManagerOverwriteQueryProduct', 'execute'];
$GLOBALS['TL_HOOKS']['sendNotificationMessage'][] = ['Bitkorn\Isitafu\HookTrailer\SendNotificationMessagePallet', 'execute'];

/**
 * Backend Module
 * 1. BE_MOD
 * 2. BE Bereich (neuen erstellen oder einen vorhandenen: system, accounts, content)
 * 3. Key für das BE Modul
 */
$GLOBALS['BE_MOD']['content']['isitafu-extension'] = [
        'isitafu' => [
            'name' => 'isitafu',
            'callback' => [
                'Bitkorn\IsitafuBundle\Backend\IsitafuBackend',
                'showBackend'
            ]
        ]
];
