<?php

$GLOBALS['TL_DCA']['tl_member_group']['palettes']['default'] = str_replace
        (
        'name;', 'name;customer_number;', $GLOBALS['TL_DCA']['tl_member_group']['palettes']['default']
);
$GLOBALS['TL_DCA']['tl_member_group']['fields']['customer_number'] = [
    'label' => &$GLOBALS['TL_LANG']['tl_member_group']['customer_number'],
    'exclude' => true,
    'inputType' => 'text',
    'eval' => array('mandatory' => true, 'rgxp' => 'digit', 'maxlength' => 10, 'unique' => true, 'tl_class' => 'w50'),
    'sql' => "varchar(10) NOT NULL default ''"
];
