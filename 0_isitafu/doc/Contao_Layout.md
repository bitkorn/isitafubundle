# Layout
## Seitenstruktur
**Seiten** haben **Edit-Buttons** wenn: URL Query Parameter do=page.
Nennen wir es **Modus page** (es gibt noch den **Modus article**). Hier sieht man keine **Artikel** oder deren **Elemente**.

Ist man im **Modus page** und klickt auf "Artikel der Seite bearbeiten" (Bleistift & Zettel) ist man im **Modus article**.
In den Modus Artikel kommt man auch mit Klick auf Artikel im linken Hauptmenü.

Im **Modus article** haben Seiten keine Edit-Buttons und man sieht Artikel oder Elemente inkl. ihrer Edit-buttons.

Es gibt genau einen Weg um aus dem Modus article in den Modus page zu kommen: Klick auf Seitenstruktur im Hauptmenü links.
Dabei bleibt man in der Hierarchie wo man ist. Es ändert sich nur der Modus.


### Modus: page
- Button: + Neue Seite
- Seiten bearbeiten
- Artikel & Elemente nicht sichtbar

### Modus: article
- Button: + neuer Artikel
- Artikel und deren Elemente bearbeiten
- Seiten ohne Edit-Buttons

### Beispiel
- Paletten (**Seite**)
    - Hauptspalte (**Artikel** - es hat Artikeldetails, Artikelteaser und Syndikation (drucken, PDF und social media Buttons))
        - Paletten zum Artikel (**Element** - Elementtyp: Modul - Modul: Paletten Listenansicht ID 593)

### Seitenstruktur Elemente
In einer Seite befinden sich Artikel welche Elemente beinhalten

####Seite
Ist Seitentyp
- Reguläre Seite
- Interne Weiterleitung
- Exerne Weiterleitung
- 403
- 404
Kann haben
- **Layout**

Links neben einer Seite (Listenansicht) ist ein Icon. Mit Klick auf dieses Öffnet sich ein neues Fenster mit diese Seite.

####Artikel

####Element
Ist Elementtyp
- verschiedene Text-Elemente
- verschiedene Katalog-Manager Dine
- verschiedene Include-Elemente (z.B. Artikel, Inhaltselement, Formular, Modul etc)

## Themes