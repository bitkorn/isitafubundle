Dieses "src" Verzeichnis ist das im Contao root (wo auch Bitkorn/ drin liegt)

Das ContaoManagerPlugin in [contao root]/app/ läd das AppBundle und es funzt.

In die Contao composer.json muß:
{
    "autoload": {
        "classmap": [
            "app/ContaoManagerPlugin.php"
        ],
        "psr-4": {
            "AppBundle\\": "src/AppBundle/"
        }
    }
}
