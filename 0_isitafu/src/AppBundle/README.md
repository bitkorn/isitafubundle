


    {
        "autoload": {
            "classmap": [
                "app/ContaoManagerPlugin.php"
            ],
            "psr-4": {
                "AppBundle\\": "src/AppBundle/"
            }
        }
    }


Plus /app/ContaoManagerPlugin.php

    public function getBundles(ParserInterface $parser)
        {
            /**
             * Wenn in der Contao composer.json unter "autoload" "psr-4" das Bundle eingetragen ist,
             * muss das hier immernoch sein (bei composer update).
             */
            return [
                BundleConfig::create(AppBundle\AppBundle::class)->setLoadAfter([ContaoCoreBundle::class]),
    //            BundleConfig::create(\Bitkorn\Isitafu\BitkornIsitafuBundle::class)->setLoadAfter([ContaoCoreBundle::class])
            ];
        }