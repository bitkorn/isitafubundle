<?php

namespace AppBundle\HookTrailer;

/**
 * Description of CatalogManagerOverwriteQueryProduct
 *
 * @author Torsten Brieskorn
 */
class CatalogManagerOverwriteQueryProduct
{

    public function execute($arrQuery, $strQuery, &$arrValues)
    {
        if (TL_MODE == 'BE') return $strQuery;

        if ($arrQuery['table'] == 'product') {
//            var_dump($arrQuery);
//            var_dump($strQuery);
//            $arrValues[4] = 0; // Hier werden die Abfrage werte für die Database->execute() Funktion übergeben.
//            var_dump($arrValues);
            $queryStock = 'IFNULL((SELECT SUM(quantity) FROM pallet WHERE product_no = product.product_no AND status IN (1,2)), 0) AS pallet_stock';
            $queryFetched = 'IFNULL((SELECT SUM(quantity) FROM pallet WHERE product_no = product.product_no AND status = 2), 0) AS pallet_fetched';
            $queryAvailable = 'IFNULL((SELECT SUM(quantity) FROM pallet WHERE product_no = product.product_no AND status = 1), 0) AS pallet_available';
            return str_replace('*', '*,' . $queryStock . ',' . $queryFetched . ',' . $queryAvailable, $strQuery);
        }

        return $strQuery;
    }

}
