<?php

namespace AppBundle\HookTrailer;

use Psr\Log\LogLevel;
use Contao\CoreBundle\Monolog\ContaoContext;
use NotificationCenter\Model\Message;
use NotificationCenter\Model\Gateway;

/**
 * https://github.com/terminal42/contao-notification_center
 *
 * @author Torsten Brieskorn
 */
class SendNotificationMessagePallet
{

    /**
     *
     * @var \Symfony\Bridge\Monolog\Logger
     */
    private $logger;

    function __construct()
    {
        $this->logger = \System::getContainer()->get('monolog.logger.contao');
//        $this->logger->log(LogLevel::INFO, 'SendNotificationMessagePallet', ['contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL)]);
//        var_dump($this->logger);
    }

    public function execute(Message $objMessage, array $arrTokens, string $language, Gateway $objGatewayModel)
    {
//        $this->logger->log(LogLevel::INFO, '$objMessage: ' . print_r($objMessage, true), ['contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL)]);
//        $this->logger->log(LogLevel::INFO, '$arrTokens: ' . print_r($arrTokens, true), ['contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL)]);
        if(!isset($arrTokens['formconfig_alias']) || $arrTokens['formconfig_alias'] != 'merkliste-abrufen') {
            return;
        }
//        $this->logger->log(LogLevel::INFO, 'SendNotificationMessagePallet', ['contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL)]);
        $dataSession = \Session::getInstance()->getData();
        if (!isset($dataSession['wishlist_pallet']) || empty($dataSession['wishlist_pallet']['table']) || $dataSession['wishlist_pallet']['table'] != 'pallet') {
            $this->logger->log(LogLevel::ALERT, 'SendNotificationMessagePallet called without pallet!',
                    ['contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL)]);
        }
        if (empty($dataSession['wishlist_pallet']['ids']) || !is_array($dataSession['wishlist_pallet']['ids'])) {
            $this->logger->log(LogLevel::ALERT, 'SendNotificationMessagePallet: no pallet ids available!',
                    ['contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL)]);
        }
        $db = \Contao\Database::getInstance();
        $success = true;
        foreach ($dataSession['wishlist_pallet']['ids'] as $id) {
            /* @var $stmt \Contao\Database\Statement */
            $result = $db->execute('UPDATE pallet SET status=2 WHERE id=' . $id);
            if ($result->affectedRows > 0) {
                $this->logger->log(LogLevel::INFO, sprintf('Pallette mit der ID %d abgerufen.', $id),
                        ['contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL)]);
            } else {
                $success = false;
                $this->logger->log(LogLevel::WARNING, sprintf('Fehler beim Abrufen der Pallette mit der ID %d.', $id),
                        ['contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL)]);
            }
//            $this->logger->log(LogLevel::DEBUG, '$result: ' . get_class($result), ['contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL)]);
        }
        if ($success) {
            \Session::getInstance()->remove('wishlist_pallet');
        }
    }

    public function execute_log(Message $objMessage, array $arrTokens, string $language, Gateway $objGatewayModel)
    {
//        $this->logger->log(LogLevel::INFO, 'SendNotificationMessagePallet', ['contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL)]);
//        $this->logger->log(LogLevel::INFO, '$objMessage: ' . print_r($objMessage, true), ['contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL)]);
        $this->logger->log(LogLevel::INFO, '$arrTokens: ' . print_r($arrTokens, true),
                ['contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL)]);
//        $this->logger->log(LogLevel::INFO, '$objGatewayModel: ' . print_r($objGatewayModel, true), ['contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL)]);
//        var_dump($objMessage);
//        var_dump($arrTokens);
//        var_dump($language);
//        var_dump($objGatewayModel);

        $objSession = \Session::getInstance();
        $this->logger->log(LogLevel::INFO, '$objSession->getData(): ' . print_r($objSession->getData(), true),
                ['contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL)]);
//        $arrIDs = $objSession->get('wishlist_tables');
//        $this->logger->log(LogLevel::INFO, 'wishlist_tables IDs (print_r): ' . print_r($arrIDs, true), ['contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL)]);
//        var_dump($arrIDs);

        $db = \Contao\Database::getInstance();
        /* @var $stmt \Contao\Database\Statement */
        $stmt = $db->prepare('SELECT * FROM product');
        $result = $stmt->query();
        $arr = $result->fetchAllAssoc();
        $this->logger->log(LogLevel::DEBUG, 'SELECT * FROM product: ' . print_r($arr, true),
                ['contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL)]);
//        var_dump($arr);
//        $this->logger->log(LogLevel::DEBUG, 'nach db Query', ['contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL)]);
    }

}
