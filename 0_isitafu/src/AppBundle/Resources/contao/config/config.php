<?php

$GLOBALS['TL_HOOKS']['replaceInsertTags'][] = array('AppBundle\Resources\contao\AppCustomTags', 'myReplaceInsertTags');
$GLOBALS['TL_HOOKS']['catalogManagerOverwriteQuery'][] = ['AppBundle\HookTrailer\CatalogManagerOverwriteQueryProduct', 'execute'];
$GLOBALS['TL_HOOKS']['sendNotificationMessage'][] = ['AppBundle\HookTrailer\SendNotificationMessagePallet', 'execute'];
