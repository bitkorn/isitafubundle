<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Description of DocumentController
 *
 * @author Torsten Brieskorn
 */
class DocumentController extends Controller
{

    /**
     *
     * @var \Symfony\Bridge\Monolog\Logger
     */
    private $logger;

    /**
     *
     * @var Doctrine\DBAL\Driver\Connection
     */
    private $connection;
    
    /**
     * Der RouteParameter kann als FunktionsParameter (string $slug) (nach Request $request) übergeben werden.
     * Oder Parameter werden per $request->get('slug') geholt.
     * 
     * http://contao-lts.local/app_dev.php/ajax/document-pk/wiederwas?c=4
     * 
     * @Route("/ajax/document-pk/{slug}")
     * @param Request $request
     * @return Response
     */
    public function documentPkAction(Request $request){
        $jsonResponse = new JsonResponse();
        $this->logger = $this->get('logger');
        
        $this->connection = $this->get('database_connection');
        $stmt = $this->connection->prepare('SELECT * FROM product WHERE id=:id');
        $id = 2;
        $stmt->bindParam(':id', $id);
        if ($stmt->execute()) {
            $this->logger->notice(print_r($stmt->fetchAll(), true));
        }

        $slug = $request->get('slug');
        $c = $request->get('c');
        
        $jsonResponse->setContent(json_encode(['$slug' => $slug, '$c' => $c]));
        return $jsonResponse;
    }
}
