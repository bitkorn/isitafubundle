<?php
/*
 * Die $GLOBALS von Contao gehören in die config.php
 * Bei 4.x also in /system/modules/ModulenameBundle/Resources/contao/config/config.php
 */

/**
 * TL_CTE
 * group: sollte der Vendor + Bundlename sein?
 * name: hier vielleicht "dingdong"
 */
$GLOBALS['TL_CTE']['group']['name'] = '\\bitkorn\\Isitafu\\ContentElement\\Dingdong';

/**
 * Ein API Hook
 * https://docs.contao.org/books/api/extensions/hooks/
 */
$GLOBALS['TL_HOOKS']['createNewUser'][] = array('MyClass', 'myCreateNewUser');
