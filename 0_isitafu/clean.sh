#!/bin/bash
# Wegen der Bequemlichkeit dies hier ins TL_ROOT :)
sudo rm -rf /var/www/html/contao-lts/var/cache
sudo rm -rf /var/www/html/contao-lts/var/logs/*
sudo -u www-data composer dump-autoload