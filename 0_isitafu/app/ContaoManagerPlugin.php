<?php

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;

class ContaoManagerPlugin implements BundlePluginInterface
{

    /**
     * {@inheritdoc} 
     */
    public function getBundles(ParserInterface $parser)
    {
        /**
         * Wenn in der Contao composer.json unter "autoload" "psr-4" das Bundle eingetragen ist,
         * muss das hier immernoch sein (bei composer update).
         * 
         * Wird das Bundle auf "normale" Weise installiert, kann das hier weg.
         * "normale" Weise:
         * 1. per composer require
         * 2. in composer.json repositories der Pfad zum Bundle und dann composer require
         */
        return [
            BundleConfig::create(AppBundle\AppBundle::class)->setLoadAfter([ContaoCoreBundle::class]),
            BundleConfig::create(\Bitkorn\Isitafu\BitkornIsitafuBundle::class)->setLoadAfter([ContaoCoreBundle::class])
        ];
    }

}
