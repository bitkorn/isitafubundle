<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$routes = new RouteCollection();
$routes->add(
        'pk_document_get'
        , new Route('/ajax/document-pk/{slug}', array(
    '_controller' => 'AppBundle\Controller\Ajax\DocumentController::documentPkAction')
        )
);

return $routes;
