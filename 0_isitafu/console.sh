#/bin/bash
# Shebang defekt weil besser nicht ausführen :)
verz=/var/www/html/contao-lts/vendor/bin
cd $verz
chmodFolder=775
chmodFile=664
chown -R www-data:allapow .
find . -type d -exec chmod $chmodFolder {} \;
find . -type f -exec chmod $chmodFile {} \;
sudo chmod -R 777 var

sudo -u www-data php contao-console cache:clear --env=dev
sudo -u www-data php contao-console cache:clear --env=prod

php contao-console route:match "/defaultindex"

## etc
sudo -u www-data composer update
sudo -u www-data composer install
sudo -u www-data composer dump-autoload

sudo rm -rf /var/www/html/contao-lts/var/cache
sudo rm -rf /var/www/html/contao-lts/var/logs/*


## unter Debian zu www-data werden
su www-data -s /bin/bash
# wieder zu root
su
