
## Routing

    ## YAML Template.
    ---
    bitkorn_isitafu_default_index:
    #    path:     /defaultindex
    #    defaults: { _controller: Bitkorn\Bundle\IsitafuBundle\Controller\DefaultController::indexAction }
    #    defaults: { _controller: Bitkorn:Isitafu:Default:index }
        resource: "@IsitafuBundle/Controller"
        type: annotation

Das auskommentierte Routing bringt einen 404.