
In /system/modules/isitafu-bundle/Resources/contao/config/config.php werden die custom ContentElements registriert.

Anschließend hat man im Contao Backend neue Inhaltselemente:
1. Seitenstruktur -> Artikel der Seite XY bearbeiten -> Artikel YZ bearbeiten -> Neues Element
2. Im neuen Element als Elementtyp gibt es die neue Kategorie "bitkorn-isitafu" und darin "dingdong" und "onlyding".

Sie werden im Frontend angezeigt :)