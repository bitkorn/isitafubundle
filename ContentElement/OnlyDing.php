<?php

namespace Bitkorn\Isitafu\ContentElement;

use Contao\ContentElement;

/**
 * Dank an https://easysolutionsit.de/artikel/contao-ein-eigenes-inhaltselement.html
 *
 * @author Torsten Brieskorn
 */
class OnlyDing extends ContentElement
{

    private $dingdongProperties = ['some_key' => 'some_prop_val'];
    
    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'fe_dingdong';

    /**
     */
    protected function compile()
    {
        if ($this->dingdongProperties != '') {
            $this->Template->arrProperties = deserialize($this->dingdongProperties, true);
        }
    }

}
