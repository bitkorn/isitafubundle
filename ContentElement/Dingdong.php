<?php

namespace Bitkorn\Isitafu\ContentElement;

use Contao\ContentElement;
use Psr\Log\LogLevel;

/**
 * Dank an https://easysolutionsit.de/artikel/contao-ein-eigenes-inhaltselement.html
 *
 * @author Torsten Brieskorn
 */
class Dingdong extends ContentElement
{

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'fe_dingdong';

    /**
     */
    protected function compile()
    {
        \System::getContainer()
                ->get('monolog.logger.contao')
                ->log(LogLevel::WARNING, 'Ein Log-Eintrag',
                        array(
                    'contao' => new \Contao\CoreBundle\Monolog\ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL
        )));
        if (TL_MODE == 'BE') {
            $this->genBeOutput();
        } else {
            $this->genFeOutput();
        }
    }

    /**
     * Erzeugt die Ausgebe für das Backend.
     * @return string
     */
    private function genBeOutput()
    {
        $this->strTemplate = 'be_dingdong';
        $this->Template = new \BackendTemplate($this->strTemplate);
        $this->Template->title = $this->headline;
        $this->Template->wildcard = "### DingDong ###";
    }

    /**
     * Erzeugt die Ausgebe für das Frontend.
     * @return string
     */
    private function genFeOutput()
    {
        if ($this->dingdongProperties != '') {
            $this->Template->arrProperties = deserialize($this->dingdongProperties, true);
        }
    }

}
