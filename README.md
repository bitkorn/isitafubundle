# Contao 4.x (4.4.x managed) Bundle
Das Isitafu Bundle erstelle ich aus Lernzwecken. Gerade wenn man beim Einstieg in ein neues System ist, hilft es mir eine
Zusammenfassung der Dinge zu haben wie sie sein sollen.

## docs

[Infoquellen für Contao-Entwickler](https://community.contao.org/de/showthread.php?53-Infoquellen-f%C3%BCr-Contao-Entwickler)

[Introduction to Contao Manager Plugins](https://docs.contao.org/books/extending-contao4/managed-edition/plugins.html)

[Sioweb/Contao4DummyBundle](https://github.com/Sioweb/Contao4DummyBundle)