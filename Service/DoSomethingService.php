<?php

namespace Bitkorn\Isitafu\Service;

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Contao\CoreBundle\Monolog\ContaoContext;
use Doctrine\DBAL\Driver\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * @author Torsten Brieskorn
 */
class DoSomethingService
{

    /**
     *
     * @var \Symfony\Bridge\Monolog\Logger
     */
    private $logger;

    /**
     *
     * @var Doctrine\DBAL\Driver\Connection
     */
    private $connection;

    /**
     *
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $container;

    /**
     *
     * @var string
     */
    private $someApiKey;

    public function __construct(LoggerInterface $logger, Connection $connection, ContainerInterface $container, string $someApiKey)
    {
        $this->logger = $logger;
        $this->connection = $connection;
        $this->container = $container;
        $this->someApiKey = $someApiKey;
    }

    public function runService(): bool
    {
        $this->contaoFramework = $this->container->get('contao.framework');
        $this->contaoFramework->initialize();
        $stmt = $this->connection->prepare('SELECT COUNT(id) AS count_member_group FROM tl_member_group');
        if (!$stmt->execute()) {
            $this->logger->log(LogLevel::ERROR, __CLASS__ . '::' . __FUNCTION__ . '() Line: ' . __LINE__ . ' can not execute SQL query.',
                    ['contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_ERROR)]);
            return false;
        }
        $row = $stmt->fetch();
        $message = 'DoSomethingService, count_member_group: ' . $row['count_member_group'];
        $this->logger->log(LogLevel::INFO, $message, ['contao' => new ContaoContext(__CLASS__ . '::' . __FUNCTION__, TL_GENERAL)]);
        return true;
    }

}
