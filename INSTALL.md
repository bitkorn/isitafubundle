
# Contao 4.4.x managed Bundle installieren

Beschrieben wird die Installation für ein, sich in Entwicklung befindendes, Bundle. Es werden also ständig Änderungen am Source Code vorgenommen.

In [ContaoRoot]/composer.json

    "repositories": [
        {
            "type": "path",
            "url": "/var/www/html/contao-lts/src/Bitkorn/IsitafuBundle",
            "options": {
                "symlink": true
            }
        }
    ],
    "require": {
        "bitkorn/isitafu-bundle": "dev-master"
    }

Das funktioniert auch mit relativer Pfadangabe:

    "repositories": [
        {
            "type": "path",
            "url": "./src/Bitkorn/IsitafuBundle",
            "options": {
                "symlink": true
            }
        }
    ],
    "require": {
        "bitkorn/isitafu-bundle": "dev-master"
    }

Der require Eintrag kommt von:

sudo -u www-data composer require bitkorn/isitafu-bundle:dev-master

Danach ein:

sudo -u www-data composer update

Dabei wird in /vendor ein Symlink (/vendor/bitkorn/isitafu-bundle) nach /src/Bitkorn/IsitafuBundle gesetzt. Dadurch werden immer die aktuellen Sourcen verwendet.


Siehe auch /IsitafuBundle/0_isitafu/composer/*

## Zeug installieren

http://contao-lts.local/contao/install