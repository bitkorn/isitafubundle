<?php

namespace Bitkorn\Isitafu\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Routing\RoutingPluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Symfony\Component\Config\Loader\LoaderResolverInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Bitkorn\Isitafu\BitkornIsitafuBundle;
use Psr\Log\LogLevel;

class Plugin implements BundlePluginInterface, RoutingPluginInterface
{

    /**
     * 
     * @param ParserInterface $parser
     * @return BundlePluginInterface
     */
    public function getBundles(ParserInterface $parser)
    {
        return [BundleConfig::create(BitkornIsitafuBundle::class)->setLoadAfter([ContaoCoreBundle::class])];
    }

    public function getRouteCollection(LoaderResolverInterface $resolver, KernelInterface $kernel)
    {
        return $resolver->resolve(__DIR__ . '/../Resources/config/routing.yml')
                        ->load(__DIR__ . '/../Resources/config/routing.yml')
        ;
    }

}
