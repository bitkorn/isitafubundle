<?php

/*
 * https://symfony.com/doc/3.4/console.html
 * https://symfony.com/doc/3.4/console/commands_as_services.html
 * Die Deppendency Injection mit LoggerInterface, um einen Logger zu haben, funktioniert nicht.
 * 
 */

namespace Bitkorn\Isitafu\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\DBAL\Driver\Connection;
use Bitkorn\Isitafu\Service\DoSomethingService;

/**
 * ContainerAwareCommand
 */
class DefaultConsoleCommand extends Command
{

    /**
     *
     * @var \Symfony\Bridge\Monolog\Logger
     */
    private $logger;

    /**
     *
     * @var Doctrine\DBAL\Driver\Connection
     */
    private $connection;

    /**
     *
     * @var \Bitkorn\Isitafu\Service\DoSomethingService
     */
    private $doSomethingService;

    /**
     * 
     * @param LoggerInterface $logger
     * @param Connection $connection
     * @param DoSomethingService $doSomethingService
     */
    public function __construct(LoggerInterface $logger, Connection $connection, DoSomethingService $doSomethingService)
    {
        $this->logger = $logger;
        $this->connection = $connection;
        $this->doSomethingService = $doSomethingService;
        // you *must* call the parent constructor
        parent::__construct();
    }

    protected function configure()
    {
        $this
                // the name of the command (the part after "bin/console")
                ->setName('BitkornIsitafuBundle:sunshine')

                // the short description shown while running "php bin/console list"
                ->setDescription('Good morning!')

                // the full command description shown when running the command with
                // the "--help" option
                ->setHelp('This command is beautifull!')
        ;
    }

    /**
     * sudo -u www-data php contao-console BitkornIsitafuBundle:sunshine
     * sudo because www-data is the owner and group of /var/logs/prod.log
     * 
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /*
         * lands in /var/logs/prod.log
         */
        $this->logger->notice('yeah mein Log from console');

        $stmt = $this->connection->prepare('SELECT * FROM product WHERE id=:id');
        $id = 2;
        $stmt->bindParam(':id', $id);
        if ($stmt->execute()) {
            $output->writeln(print_r($stmt->fetchAll(), true));
        }

        echo 'blah' . PHP_EOL;
        if ($this->doSomethingService->runService()) {
            echo 'DoSomethingService success' . PHP_EOL;
        } else {
            echo 'DoSomethingService failed' . PHP_EOL;
        }
    }

}
