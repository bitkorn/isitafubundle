<?php

/*
 * 
 */

namespace Bitkorn\Isitafu\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Contao\CoreBundle\Framework\ContaoFramework;

/**
 * &commat;(@)Route(defaults={"_scope" = "backend", "_token_check" = true})
 * ...funzt auch (dazu in der routing.yml type: annotation benutzen) ...der path dann an der *Action()
 * @Route(defaults={"_scope" = "backend", "_token_check" = true}) 
 */
class DefaultController extends Controller
{

    /**
     * Schreibt ins File und nicht ins Contao Backend.
     * debug() wird nicht geschrieben ...wo sind dazu die Einstellungen?
     *
     * @var \Symfony\Bridge\Monolog\Logger
     */
    private $logger;

    /**
     *
     * @var Doctrine\DBAL\Driver\Connection
     */
    private $connection;

    /**
     *
     * @var Symfony\Component\HttpFoundation\Session\SessionInterface 
     */
    private $objSession;
    
    /**
     *
     * @var \Contao\CoreBundle\Framework\ContaoFramework
     */
    private $contaoFramework;
    
    /**
     *
     * @var \Contao\FrontendUser
     */
    private $frontendUser;
    
    /**
     *
     * @var \Contao\BackendUser
     */
    private $backendUser;

    /**
     * Der RouteParameter kann als FunktionsParameter (string $slug) übergeben werden.
     * Oder Parameter werden per $request->get('slug') geholt.
     * 
     * http://contao-lts.local/app_dev.php/defaultindex/wiederwas?c=4
     * 
     * Entweder einzelne Routen in der routing.yml (/src/Bitkorn/IsitafuBundle/Resources/config/routing.yml)
     * oder hier per Annotation: &commat;(@)Route("/defaultindex/{slug}")
     * und in der routing.yml type: annotation benutzen.
     * 
     * @Route("/defaultindex/{slug}")
     * @param Request $request
     * @param SessionInterface $objSession
     * @return Response
     */
    public function indexAction(Request $request, SessionInterface $objSession)
    {
        $response = new Response();
        $this->logger = $this->get('logger'); // deprecated, aber per DI gibts n fehler
//        $this->logger->notice('yeah mein Log mit $slug: ' . $slug); // vom FunktionsParameter
        $this->objSession = $objSession; // ohne session_start() das selbe (zu wenig) drin
//        session_start(); // wenn man $_SESSION benutzen will
//        $session = \Contao\Session::getInstance(); // ClassNotFoundException in \Contao\Session
//        $session = $this->get('session'); // nix drin

        $this->connection = $this->get('database_connection');
        $stmt = $this->connection->prepare('SELECT * FROM product WHERE id=:id');
        $id = 2;
        $stmt->bindParam(':id', $id);
        if ($stmt->execute()) {
            $this->logger->notice(print_r($stmt->fetchAll(), true));
        }

        $slug = $request->get('slug');
        $c = $request->get('c');
        
        $html = '';
        $html .= '<textarea cols="100" rows="20">' . print_r($this->objSession->all(), true) . '</textarea>';
        $html .= '<textarea cols="100" rows="20">' . print_r($_SESSION, true) . '</textarea>';
        $html .= '<br>RouteParam: ' . $slug . '<br>QueryParam: ' . $c;
        $response->setContent($html);
        return $response;
    }


    /**
     * http://contao-lts.local/app_dev.php/usersessiontest
     * 
     * Entweder einzelne Routen in der routing.yml (/src/Bitkorn/IsitafuBundle/Resources/config/routing.yml)
     * oder hier per Annotation: &commat;(@)Route("/usersessiontest", defaults={"_scope" = "backend"})
     * und in der routing.yml type: annotation benutzen.
     * 
     * &commat;(@)Route("/usersessiontest", defaults={"_scope" = "backend"})
     * @Route("/usersessiontest")
     * @param Request $request
     * @param SessionInterface $objSession
     * @return Response
     */
    public function userSessionTestAction(Request $request, SessionInterface $objSession)
    {
        $this->objSession = $objSession;
        $response = new Response();
//        session_start(); // wenn man $_SESSION benutzen will

        $this->contaoFramework = $this->get('contao.framework');
        $this->contaoFramework->initialize();
        $this->frontendUser = \Contao\FrontendUser::getInstance();
        $this->backendUser = \Contao\BackendUser::getInstance();
//        $objUser = $this->get('BackendUser'); // You have requested a non-existent service "BackendUser".
        
        $html = '';
//        $html .= '<textarea cols="100" rows="20">' . print_r($this->objSession->all(), true) . '</textarea>';
//        $html .= '<textarea cols="100" rows="20">' . print_r($_SESSION, true) . '</textarea>';
        $html .= '<br>$this->frontendUser->id: ' . $this->frontendUser->id;
        $html .= '<br>$this->backendUser->getData(): <textarea cols="100" rows="20">' . print_r($this->backendUser->getData(), true) . '</textarea>';
        $response->setContent($html);
        return $response;
    }
}
